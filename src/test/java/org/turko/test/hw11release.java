package org.turko.test;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class hw11release {
    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

    @BeforeMethod
    public void beforeMethod() {
        driver.get("http://the-internet.herokuapp.com/javascript_alerts");
    }

    @Test
    public void jsConfirmOk() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Ok";

        clickJSButton(Buttons.JS_CONFIRM);
        String text = workWithAlert(true);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);
    }

    @Test
    public void jsConfirmTestCancel() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Cancel";

        clickJSButton(Buttons.JS_CONFIRM);
        String text = workWithAlert(false);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);
    }

    @Test
    public void jsPromptEnterText() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "Test!";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(true, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + testToEnter, resultText);
    }

    @Test
    public void jsPromptWithoutEnterText() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(true, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered:" + testToEnter, resultText);
    }

    @Test
    public void jsPromptEnterTextCancel() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "Test!";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(false, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + null, resultText);

    }

    @Test
    public void jsPromptWithoutEnterTextCancel() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(false, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + null, resultText);
    }


    private void clickJSButton(Buttons buttons) {
        findButton(buttons).click();
    }

    private WebElement findButton(Buttons button) {
        return driver.findElement(By.xpath("//button[text()='%s']".formatted(button.getTextValue())));
    }

    private String workWithAlert(boolean accept, String... textToInput) {
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();

        if (textToInput.length > 0) {
            alert.sendKeys(textToInput[0]);
        }
        if (accept) {
            alert.accept();
        } else {
            alert.dismiss();
        }
        return text;
    }

    private String getResultText() {
        return driver.findElement(By.id("result")).getText();
    }

    enum Buttons {
        JS_CONFIRM("Click for JS Confirm"),
        JS_PROMPT("Click for JS Prompt");

        private String textValue;

        Buttons(String textValue) {
            this.textValue = textValue;
        }

        public String getTextValue() {
            return textValue;
        }
    }
}