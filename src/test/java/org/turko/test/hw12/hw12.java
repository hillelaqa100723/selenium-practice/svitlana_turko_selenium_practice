package org.turko.test.hw12;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class hw12 {
    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

    @BeforeMethod
    public void beforeMethod() {
        driver.get("http://the-internet.herokuapp.com/javascript_alerts");
    }

    @Test
    public void jsConfirmOk() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Ok";

        clickJSButton(Buttons.JS_CONFIRM);
        String text = workWithAlert(true);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);
    }

    @Test
    public void jsConfirmOkWithJs() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Ok";

        clickJSButtonWithJs(Buttons.JS_CONFIRM);
        String text = workWithAlert(true);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);
    }

    @Test
    public void jsConfirmTestCancel() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Cancel";

        clickJSButton(Buttons.JS_CONFIRM);
        String text = workWithAlert(false);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);
    }

    @Test
    public void jsConfirmTestCancelJs() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Cancel";

        clickJSButtonWithJs(Buttons.JS_CONFIRM);
        String text = workWithAlert(false);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);
    }

    @Test
    public void jsPromptEnterText() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "Test!";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(true, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + testToEnter, resultText);
    }

    @Test
    public void jsPromptEnterTextJs() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "Test!";

        clickJSButtonWithJs(Buttons.JS_PROMPT);
        String textFromAlert = workWithAlert(true, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + testToEnter, resultText);
    }

    @Test
    public void jsPromptWithoutEnterText() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(true, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered:" + testToEnter, resultText);
    }

    @Test
    public void jsPromptWithoutEnterTextJs() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "";

        clickJSButtonWithJs(Buttons.JS_PROMPT);
        String textFromAlert = workWithAlert(true, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered:" + testToEnter, resultText);
    }

    @Test
    public void jsPromptEnterTextCancel() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "Test!";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(false, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + null, resultText);

    }

    @Test
    public void jsPromptEnterTextCancelJs() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "Test!";

        clickJSButtonWithJs(Buttons.JS_PROMPT);
        String textFromAlert = workWithAlert(false, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + null, resultText);

    }

    @Test
    public void jsPromptWithoutEnterTextCancel() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(false, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + null, resultText);
    }

    @Test
    public void jsPromptWithoutEnterTextCancelJs() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "";

        clickJSButtonWithJs(Buttons.JS_PROMPT);
        String textFromAlert = workWithAlert(false, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + null, resultText);
    }

    private void clickJSButton(Buttons buttons) {
        findButton(buttons).click();
    }

    private void clickJSButtonWithJs(Buttons buttons) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("return " + buttons.getCode());
    }

    private WebElement findButton(Buttons button) {
        return driver.findElement(By.xpath("//button[text()='%s']".formatted(button.getTextValue())));
    }

    private String workWithAlert(boolean accept, String... textToInput) {
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();

        if (textToInput.length > 0) {
            alert.sendKeys(textToInput[0]);
        }
        if (accept) {
            alert.accept();
        } else {
            alert.dismiss();
        }
        return text;
    }

    private String getResultText() {
        return driver.findElement(By.id("result")).getText();
    }

    private String getResultTextWithJs() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        WebElement element = driver.findElement(By.id("result"));
        return executor.executeScript("return arguments[0].textContent;", element).toString();
    }

    enum Buttons {
        JS_CONFIRM("Click for JS Confirm", "jsConfirm()"),
        JS_PROMPT("Click for JS Prompt", "jsPrompt()");

        private String textValue;
        private String code;

        Buttons(String textValue, String code) {
            this.textValue = textValue;
            this.code = code;
        }

        public String getTextValue() {
            return textValue;
        }

        public String getCode() {
            return code;
        }
    }
}
