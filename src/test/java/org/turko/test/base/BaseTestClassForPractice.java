package org.turko.test.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

public class BaseTestClassForPractice {
    protected WebDriver driver;
    @BeforeSuite
    public void beforeClass(){
        driver = new ChromeDriver();
    }

    @AfterSuite(alwaysRun = true)
    public void afterClass() {
        if(driver != null) {
            driver.quit();
        }
    }
}