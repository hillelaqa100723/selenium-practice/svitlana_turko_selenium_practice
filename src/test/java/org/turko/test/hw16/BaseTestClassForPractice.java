package org.turko.test.hw16;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.turko.driver.WebDriverFactory;
import org.turko.utils.MyFileUtils;

public class BaseTestClassForPractice {
    protected WebDriver driver;

    @BeforeSuite
    public void beforeClass() {
        MyFileUtils.getDownloadsFolder();
        driver = WebDriverFactory.initDriver();
    }

    @AfterSuite(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }
}